FROM openjdk:18-alpine3.13
WORKDIR /app
COPY . .
RUN javac Server.java

ENV PORT=5000
EXPOSE 5000
CMD ["java", "Server"]

