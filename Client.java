import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;


public class Client {
    private static KeyPair keyPairGenerate() throws NoSuchAlgorithmException {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
        generator.initialize(2048);

        return generator.generateKeyPair();
    }

    public static void main(String[] args) throws MalformedURLException {
        final String PUBLIC_KEY_HEADER = "-----BEGIN PUBLIC KEY-----\n";
        final String PUBLIC_KEY_FOOTER = "-----END PUBLIC KEY-----\n";
        final String PRIVATE_KEY_HEADER = "-----BEGIN PRIVATE KEY-----\n";
        final String PRIVATE_KEY_FOOTER = "-----END PRIVATE KEY-----\n";
        final String CERTIFICATE_HEADER = "-----BEGIN CERTIFICATE-----\n";
        final String CERTIFICATE_FOOTER = "-----END CERTIFICATE-----\n";
        String serverUrl = System.getenv("SERVER_URL");
        Key issuerPublicKey;
        if (serverUrl == null) {
            serverUrl = "localhost:9999";
        }
        KeyPair keyPair = null;
        try {
            keyPair = keyPairGenerate();
            System.out.println("Public Key of Client");
            System.out.println(Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded()));
            System.out.println("");

            System.out.println("Private Key of Client");
            System.out.println(Base64.getEncoder().encodeToString(keyPair.getPrivate().getEncoded()));
            System.out.println("");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }

        String[] strings = serverUrl.split(":");
        String host = strings[0];
        int port = Integer.parseInt(strings[1]);

        Socket socketOfClient = null;
        BufferedWriter os = null;
        BufferedReader is = null;

        try {
            socketOfClient = new Socket(host, port);

            // T?o lu?ng d?u ra t?i client (G?i d? li?u t?i server)
            os = new BufferedWriter(new OutputStreamWriter(socketOfClient.getOutputStream()));

            // Lu?ng d?u vào t?i Client (Nh?n d? li?u t? server).
            is = new BufferedReader(new InputStreamReader(socketOfClient.getInputStream()));

        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + serverUrl);
            return;
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " + serverUrl);
            return;
        }

        try {
            Scanner scanner = new Scanner(System.in);
            // Enter ID
            UUID uuid = UUID.randomUUID();
            System.out.print("Name (" + uuid +"): ");
            String name = scanner.nextLine();
            if (name.isEmpty()) name = uuid.toString();

//            // Send ID and Public Key
//            System.out.print("Public Key: ");
//            String publicKey = scanner.nextLine();

            // 1. Fetch public key of issuer server
            os.write("get-public-key"); os.newLine(); os.flush();
            String issuerPublicKeyInBase64 = is.readLine();
            System.out.println("Public Key of Server");
            System.out.println(issuerPublicKeyInBase64);
            System.out.println("");
            System.out.println("");
            {
                byte[] encoded = Base64.getDecoder().decode(issuerPublicKeyInBase64);
                KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encoded);
                issuerPublicKey = (RSAPublicKey) keyFactory.generatePublic(keySpec);
            }

            // 2. Request issuer to create a certificate
            System.out.println("Press enter to send Name and Public Key of Client to Server");
            scanner.nextLine();
            os.write(String.format("create-certificate,%s,%s", name, Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded()))); os.newLine(); os.flush();
            String certificateInBase64 = is.readLine();
            System.out.println("Certificate of Client created by Server");
            System.out.println(certificateInBase64);
            System.out.println("");
            System.out.println("");
            byte[] certificate = Base64.getDecoder().decode(certificateInBase64);

            // 3. Save keys to file
            PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(
                    keyPair.getPrivate().getEncoded());
            FileOutputStream fos = new FileOutputStream("client.private.key");
//            fos.write(pkcs8EncodedKeySpec.getEncoded());
            String encoded = Base64.getEncoder().encodeToString(pkcs8EncodedKeySpec.getEncoded());
            fos.write(PRIVATE_KEY_HEADER.getBytes());
            for (int i = 0; i < encoded.length(); i+=64) {
                fos.write(encoded.substring(i, i+Math.min(64, encoded.length()-i)).getBytes());
                fos.write("\n".getBytes());
            }
            fos.write(PRIVATE_KEY_FOOTER.getBytes());
            fos.close();
            System.out.println("Saved private key of client to file client.private.key");

            X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(
                    keyPair.getPublic().getEncoded());
            fos = new FileOutputStream("client.public.key");
            encoded = Base64.getEncoder().encodeToString(x509EncodedKeySpec.getEncoded());
            fos.write(PUBLIC_KEY_HEADER.getBytes());
            for (int i = 0; i < encoded.length(); i+=64) {
                fos.write(encoded.substring(i, i+Math.min(64, encoded.length()-i)).getBytes());
                fos.write("\n".getBytes());
            }
            fos.write(PUBLIC_KEY_FOOTER.getBytes());
            fos.close();
            System.out.println("Saved public key of client to file client.public.key");

            fos = new FileOutputStream("client.ca");
            encoded = certificateInBase64;
            fos.write(CERTIFICATE_HEADER.getBytes());
            for (int i = 0; i < encoded.length(); i+=64) {
                fos.write(encoded.substring(i, i+Math.min(64, encoded.length()-i)).getBytes());
                fos.write("\n".getBytes());
            }
            fos.write(CERTIFICATE_FOOTER.getBytes());
            fos.close();
            System.out.println("Saved certificate key of client to file client.ca");

            // 4. Verify certificate (decrypt certificate using issuer public key)
            Cipher decryptCipher = Cipher.getInstance("RSA");
            decryptCipher.init(Cipher.DECRYPT_MODE, issuerPublicKey);
            byte[] decryptedMessageBytes = decryptCipher.doFinal(certificate);
            String decryptedMessage = new String(decryptedMessageBytes, StandardCharsets.UTF_8);
            args = decryptedMessage.split(",");
            Date date = new Date(Integer.parseInt(args[0], 10) * 1000L);
            name = args[1];

            System.out.println();
            System.out.println("Read certificate");
//            System.out.println(decryptedMessage);
            System.out.println("Certificate is issued for " + name + " at " + date);
            System.out.println("");
            System.out.println("");

            os.write(Arrays.toString(keyPair.getPublic().getEncoded()));
            os.newLine();
            os.flush();

            // notice server that we will close connection
            os.write("close"); os.newLine(); os.flush();

            os.close();
            is.close();
            socketOfClient.close();
        } catch (UnknownHostException | InvalidKeySpecException e) {
            System.err.println("Trying to connect to unknown host: " + e);
        } catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException |
                 BadPaddingException | InvalidKeyException e) {
            System.err.println("IOException:  " + e);
        }
    }

}
