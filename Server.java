import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;

public class Server {

    private KeyPair keyPair;
    private static String PUBLIC_KEY_FILE_NAME="issuer.pub";
    private static String PRIVATE_KEY_FILE_NAME="issuer.pri";

    private static KeyPair KeyPairGenerate() throws NoSuchAlgorithmException {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
        generator.initialize(4096);

        return generator.generateKeyPair();
    }

    public static void SavePublicKey(RSAPublicKey key, String filename) throws IOException {
        final String PUBLIC_KEY_HEADER = "-----BEGIN PUBLIC KEY-----\n";
        final String PUBLIC_KEY_FOOTER = "-----END PUBLIC KEY-----\n";
        FileOutputStream fos = new FileOutputStream(filename);
        String encoded = Base64.getEncoder().encodeToString(key.getEncoded());
        fos.write(PUBLIC_KEY_HEADER.getBytes());
        for (int i = 0; i < encoded.length(); i+=64) {
            fos.write(encoded.substring(i, i+Math.min(64, encoded.length()-i)).getBytes());
            fos.write("\n".getBytes());
        }
        fos.write(PUBLIC_KEY_FOOTER.getBytes());
        fos.close();
    }

    public static void SavePrivateKey(RSAPrivateKey key, String filename) throws IOException {
        final String PRIVATE_KEY_HEADER = "-----BEGIN PRIVATE KEY-----\n";
        final String PRIVATE_KEY_FOOTER = "-----END PRIVATE KEY-----\n";
        FileOutputStream fos = new FileOutputStream(filename);
        String encoded = Base64.getEncoder().encodeToString(key.getEncoded());
        fos.write(PRIVATE_KEY_HEADER.getBytes());
        for (int i = 0; i < encoded.length(); i+=64) {
            fos.write(encoded.substring(i, i+Math.min(64, encoded.length()-i)).getBytes());
            fos.write("\n".getBytes());
        }
        fos.write(PRIVATE_KEY_FOOTER.getBytes());
        fos.close();
    }

    public static RSAPublicKey LoadPublicKey(String filename) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        String key = Files.readString(Paths.get(filename), Charset.defaultCharset());

        String publicKeyPEM = key
                .replace("-----BEGIN PUBLIC KEY-----", "")
                .replaceAll(System.lineSeparator(), "")
                .replace("-----END PUBLIC KEY-----", "");

        byte[] encoded = Base64.getDecoder().decode(publicKeyPEM);

        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encoded);

        return (RSAPublicKey) keyFactory.generatePublic(keySpec);
    }

    public static RSAPrivateKey LoadPrivateKey(String filename) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        String key = Files.readString(Paths.get(filename), Charset.defaultCharset());

        String publicKeyPEM = key
                .replace("-----BEGIN PRIVATE KEY-----", "")
                .replaceAll(System.lineSeparator(), "")
                .replace("-----END PRIVATE KEY-----", "");

        byte[] encoded = Base64.getDecoder().decode(publicKeyPEM);

        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);

        return (RSAPrivateKey) keyFactory.generatePrivate(keySpec);
    }

    private static KeyPair GetKeyPair() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        KeyPair keyPair = null;
        {
            RSAPublicKey publicKey = null;
            {
                File f = new File(PUBLIC_KEY_FILE_NAME);
                if (f.exists() && !f.isDirectory()) {
                    publicKey = LoadPublicKey(PUBLIC_KEY_FILE_NAME);
                }
            }

            RSAPrivateKey privateKey = null;
            {
                File f = new File(PRIVATE_KEY_FILE_NAME);
                if (f.exists() && !f.isDirectory()) {
                    privateKey = LoadPrivateKey(PRIVATE_KEY_FILE_NAME);
                }
            }

            if (publicKey != null && privateKey != null) {
                keyPair = new KeyPair(publicKey, privateKey);
            }
        }

        if (keyPair == null) {
            keyPair = KeyPairGenerate();
            SavePublicKey((RSAPublicKey) keyPair.getPublic(), PUBLIC_KEY_FILE_NAME);
            SavePrivateKey((RSAPrivateKey) keyPair.getPrivate(), PRIVATE_KEY_FILE_NAME);
        }

        return keyPair;
    }

    public static void main(String args[]) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        ServerSocket listener = null;
        int clientNumber = 1;
        String portStr = System.getenv("PORT");
        int port = portStr == null || portStr.isEmpty() ? 9999 : Integer.parseInt(portStr);

        try {
            listener = new ServerSocket(port);
        } catch (IOException e) {
            System.out.println(e);
            System.exit(1);
        }

        KeyPair keyPair = GetKeyPair();
        System.out.println("Public Key of Server");
        String encoded = Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded());
        System.out.println(encoded);
//        System.out.println(encoded.substring(0, 16) + "..." + encoded.substring(encoded.length()-16, encoded.length()));
        System.out.println("");

        System.out.println("Private Key of Server");
        System.out.println(Base64.getEncoder().encodeToString(keyPair.getPrivate().getEncoded()));
        System.out.println("");

        System.out.println("Waiting for client connection on port " + port + " ...");

        try {
            while (true) {
                // Chấp nhận một yêu cầu kết nối từ phía Client.
                // Đồng thời nhận được một đối tượng Socket tại server.

                Socket socketOfServer = listener.accept();
                new ServiceThread(socketOfServer, clientNumber++, keyPair).start();
            }
        } finally {
            listener.close();
        }
    }
    private static void log(String message) {
        System.out.println(message);
    }
    private static class ServiceThread extends Thread {
        private int clientNumber;
        private Socket socketOfServer;
        private final KeyPair keyPair;

        public ServiceThread(Socket socketOfServer, int clientNumber, KeyPair keyPair) {
            this.clientNumber = clientNumber;
            this.socketOfServer = socketOfServer;
            this.keyPair = keyPair;
            // Log
            log("Connected client #" + this.clientNumber + " at " + socketOfServer);
        }

        private String createCertificate(String name, String publicKey) {
            long now = System.currentTimeMillis() / 1000;
            String iss = "";
            long iat = now;
            long exp = now + 3600;
            String payload = String.format("%d,%s,%s", iat, name, publicKey);


            Cipher encryptCipher = null;
            byte[] encryptedMessageBytes;
            try {
                encryptCipher = Cipher.getInstance("RSA");
                encryptCipher.init(Cipher.ENCRYPT_MODE, keyPair.getPrivate());

                byte[] secretMessageBytes = payload.getBytes(StandardCharsets.UTF_8);
                encryptedMessageBytes = encryptCipher.doFinal(secretMessageBytes);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            return Base64.getEncoder().encodeToString(encryptedMessageBytes);
        }

        @Override
        public void run() {
            try {
                // Mở luồng vào ra trên Socket tại Server.
                BufferedReader is = new BufferedReader(new InputStreamReader(socketOfServer.getInputStream()));
                BufferedWriter os = new BufferedWriter(new OutputStreamWriter(socketOfServer.getOutputStream()));
                while (true) {
                    // Đọc dữ liệu tới server (Do client gửi tới).
                    String line = is.readLine();
                    if (line == null || line.length() == 0) continue;
                    String[] args = line.split(",");
                    String cmd = args[0];

                    switch (cmd) {
                        case "create-certificate": {
                            System.out.println(cmd);
                            String name = args[1];
                            String clientPublicKey = args[2];
                            String certificate = createCertificate(name, clientPublicKey);
                            System.out.println(certificate);
                            os.write(certificate);
                            break;
                        }
                        case "get-public-key": {
                            System.out.println(cmd);
                            os.write(Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded()));
                            break;

                        }
                        case "close": {
                            // do nothing
                            break;
                        }

                        default: os.write("unknown command");
                    }
                    os.newLine();
                    os.flush();

                    if (cmd.equals("close")) {
                        // close connection
                        os.close();
                        System.out.println("Disconnected client #" + this.clientNumber + " at " + this.socketOfServer);
                        break;
                    }
                }
            } catch (Exception e) {
                System.out.println(e);
                e.printStackTrace();
            } finally {
            }
        }
    }
}
